#!/usr/bin/env bash
if test $# -eq 0; then
	echo $0 project_name
	exit 1
fi

cookiecutter --no-input "bb:Anders/ansible-pre" project=$1
