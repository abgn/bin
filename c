#!/usr/bin/env python3
from getpass import getuser
from os import getenv
from shlex import split
from subprocess import call
from sys import argv

cfg=getenv('HOME') + '/.calendar/work'
cmd='agenda'

if len(argv[1:]) > 0:
    if argv[1] == 'quick':
        cmd='quick "' + ' '.join(argv[2:]) + '"'
    else:
        cmd=' '.join(argv[1:])

call(split('gcalcli --config-folder {} --calendar {} --calendar alarmduty#yellow {} --military'.format(cfg, getuser(), cmd)))
